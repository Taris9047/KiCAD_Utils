#!/usr/bin/env python
#
# SwapLayer.py
#
# Swaps layers for pcbnew file. This script was written since KiCAD 8's pcbnew isn't cable of swapping layers without crashing out..
# 

import os
import sys
import argparse
import tempfile

temp_layer_name = '__Xx_temp_layer_xX__'

entire_layer_list = []

class SwapLayer(object):

    pcbnew_file_lines = []
    # pcbnew_file_lines_backup = []
    layers_to_swap = ()
    swapped_lines = []
    verbose_layers = False
    verbose_inf = True
    verbose_outf = True

    def __init__(self, pcbnew_file='', layers_to_swap='', outf_name='', 
                verbose_layers=True, verbose_inf=True, verbose_outf=True):

        self.verbose_layers = verbose_layers
        self.verbose_outf = verbose_outf
        self.verbose_inf = verbose_inf

        # Read in the pcbnew file...
        try: 
            with open(pcbnew_file, 'r') as file:
                if self.verbose_inf:
                    print(f"Reading in '{pcbnew_file}'...")
                
                #lns = file.readlines()
                #self.pcbnew_file_lines = [ ln.rstrip(os.linesep) for ln in lns ]
                self.pcbnew_file_lines = file.readlines()

                if not self.pcbnew_file_lines[0].startswith("(kicad_pcb"):
                    print(f"File '{pcbnew_file}' is not a valid Pcbnew file!!")
                    sys.exit(1)

                # Could be just a waste of memory. 
                self.swapped_lines = []

                self.layer_def_lines = []
                self.layer_numbers = []
                self.layer_names = []
                self.layer_roles = []
                self.layer_long_name = []
                layer_def_line = False
                for ind, ln in enumerate(self.pcbnew_file_lines):
                    if not ln:
                        continue

                    if ln.startswith('\t(layers'):
                        layer_def_line = True
                        continue

                    if ln.startswith('\t\t(setup'):
                        layer_def_line = True
                        continue

                    if layer_def_line == True and ln.startswith('\t)'):
                        layer_def_line = False
                        continue
                
                    if layer_def_line:
                        self.layer_def_lines.append(ind)
                        ln_parts = ln.replace('(','').replace(')','').replace('\t','').split(' ')
                        self.layer_numbers.append( int(ln_parts[0]) )
                        self.layer_names.append( ln_parts[1] )
                        self.layer_roles.append( ln_parts[2] )
                        try: 
                            self.layer_long_name.append( ln_parts[3] )
                        except:
                            self.layer_long_name.append('None')
    
        except FileNotFoundError:
            print(f"File '{pcbnew_file}' not found!")
            sys.exit(1)

        # Parse layers to swap.        
        if not '_' in layers_to_swap:
            print(f"Layer delimiter is not valid. '{layers_to_swap}' Please use '_'")
            sys.exit(1)
        else:
            self.layers_to_swap = tuple(layers_to_swap.strip().split('_'))
            print(f"Swapping Layers: '{self.layers_to_swap[0]}' and '{self.layers_to_swap[1]}'")
        
        # Checking if swap list is the same...
        if self.layers_to_swap[0] == self.layers_to_swap[1]:
            print(f"Swapping same layers? No reason to do it.")
            sys.exit(0)

        # Checking if a layer has been swapped in the session... If it exists, we may have trouble...
        # for l in self.layers_to_swap:
        #     if l in entire_layer_list:
        #         user_input = input(f"Layer '{l}' has been swapped before... This could be messy. Continue? [Y/n]")
        #         if user_input.lower().startswith('y'):
        #             continue
        #         else:
        #             print("Exiting")
        #             sys.exit(1)
                
        #     else:
        #         entire_layer_list.append(l)

        # Set up output file...
        self.outf_name = pcbnew_file+f".swap.{layers_to_swap}"
        if outf_name:
            self.outf_name = outf_name

        if self.verbose_layers:
            self.PrintLayers()

        self.RunSwap()

    """
        Actually runs the layer swap operation.

    """
    def RunSwap(self):
        layer1, layer2 = self.layers_to_swap

        if not layer1 or not layer2:
            print(f"'{layer1}' and '{layer2}' both needs to be a valid layer!! Please use KiCAD's layer name convention. For example: In2.Cu, F.Cu, etc.")
            sys.exit(1)
        
        occurance1 = 0
        occurance2 = 0
        for ln in self.pcbnew_file_lines:
            if layer1 in ln:
                occurance1 += 1
            if layer2 in ln:
                occurance2 += 1
        
        if occurance1 == 0 or occurance2 == 0:
            print(f"'{layer1}' or '{layer2}' does not exist in the file... unable to swap!")
            sys.exit(1)
        
        print(f"Swapping '{layer1}' and '{layer2}' of occurances: {occurance1} and {occurance2}")

        # Now actually swap the layers...
        tmp_layer_name1 = temp_layer_name + '1__'
        tmp_layer_name2 = temp_layer_name + '2__'

        temp_lines = []
        for ind, ln in enumerate(self.pcbnew_file_lines):
            
            line = ln
            if not ind in self.layer_def_lines:
                if "(layer " in ln or "(layers " in ln:
                    if layer1 in ln:
                        line = ln.replace(layer1, tmp_layer_name1)
                    if layer2 in ln:
                        line = ln.replace(layer2, tmp_layer_name2)

            temp_lines.append(line)
        
        self.swapped_lines = []
        for ind, ln in enumerate(temp_lines):
            
            line = ln
            if not ind in self.layer_def_lines:
                if "(layer " in ln or "(layers " in ln:
                    if tmp_layer_name1 in ln:
                        line = ln.replace(tmp_layer_name1, layer2)
                    
                    if tmp_layer_name2 in ln:
                        line = ln.replace(tmp_layer_name2, layer1)
            
            self.swapped_lines.append(line)

        temp_liens = None
        
        if len(self.swapped_lines) > 0:
            if self.verbose_outf:
                print(f"Saving swapped pcbnew file: '{self.outf_name}'")
            
            with open(self.outf_name, 'w') as fp:
                # fp.writelines(f"{os.linesep}".join(self.swapped_lines))
                fp.writelines(self.swapped_lines)

    """
        Prints out layer summary into stdout.

    """
    def PrintLayers(self):
        for lnum, ln, lrole, lln in zip(self.layer_numbers, self.layer_names, self.layer_roles, self.layer_long_name):
            print(f"Layer #: {lnum}, Name: {ln}, Role: {lrole}, Long Name: {lln}")


#
# Swap multiple set of layers
#
def batch_swap(layer_list, source_file_name):
    with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
        tmp_outf_name = tmp_file.name

    for layer_couple in layer_list:
        if layer_couple == layer_list[0]:
            in_file_name = source_file_name
            verbose_layer_list = True
            verbose_infile_name = True
            verbose_outf_name = False
        else:
            in_file_name = tmp_outf_name
            verbose_layer_list = False
            verbose_infile_name = False

        if layer_couple == layer_list[-1]:
            outf_name = source_file_name + '.swap.' + '__'.join(layer_list)
            verbose_outf_name = True
        else:
            outf_name = tmp_outf_name

        SwpLayer = SwapLayer(
            pcbnew_file=in_file_name, 
            layers_to_swap=layer_couple, 
            outf_name=outf_name,
            verbose_layers=verbose_layer_list,
            verbose_inf=verbose_infile_name,
            verbose_outf=verbose_outf_name)
        
    os.remove(tmp_outf_name)  

def read_in_layer_swap_table(layer_swap_filename, delims=[',', '/t', ' ', ';']):
    try:
        swap_layer_list = []
        with open(layer_swap_filename, 'r') as fp:
            lns = fp.readlines()
            lines = [ l.replace('  ',' ').replace('/t/t', '/t').rstrip() for l in lns ]
            for l in lines:
                for d in delims:
                    l = l.replace(d, '_')

                if not l.strip():
                    continue

                if l.startswith("#"):
                    continue
                
                lsep = l.split('_')
                if lsep[-1] == '':
                    lsep = lsep[:-1]
                if len(lsep) > 2:
                    lsep = lsep[:2]
                
                swap_layer_list.append('_'.join(lsep))
        
        return swap_layer_list
        
    except FileNotFoundError:
        print(f"Layer swap table '{layer_swap_filename}' not found!")
        sys.exit(1)

#
# The main function
#
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Swaps Layers for pcbnew!!")    
    parser.add_argument('first', help='Pcbnew filename')
    parser.add_argument('-l', '--layers', action='append', help="Layers to swap, delimited with '_'. For example, --layers In2.Cu_In4.Cu")
    parser.add_argument('-o', '--output', help="Output file name. '<original>.kicad_pcb.swap.layers_info' is default.")
    parser.add_argument('-lt', '--layertable', help="Layer table file. Can be delimited with space, tab, comma, semicolon.")

    args = parser.parse_args()

    source_file = args.first
    output_file = args.output

    if args.layers != None and len(args.layers) == 1:
        SwpLayer = SwapLayer(
            pcbnew_file=source_file, 
            layers_to_swap=args.layers[0], 
            outf_name=output_file)
    elif args.layertable:
        layer_list = read_in_layer_swap_table(args.layertable)
        print(layer_list)
        batch_swap(layer_list, args.first)

    else:
        batch_swap(args.layers, args.first)

         