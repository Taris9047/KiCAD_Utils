#!/usr/bin/env python

import os
import sys
import zipfile
import shutil
import configparser
from datetime import datetime as dt

# KiCAD related packages...
import pcbnew
import wx

sys.dont_write_bytecode = True

config_file_name = 'package_fab_output.config.ini'

class PKGFOutput(object):

	def __init__(self):
		board = pcbnew.GetBoard()
		pcb_file_name = board.GetFileName()
		self.current_dir = os.path.dirname(os.path.realpath(pcb_file_name))

		self.archive_type = 'zip'

		self.Project_list = []

		# Get project names from this directory.
		file_list = [ f for f in os.listdir(self.current_dir) if os.path.isfile(os.path.join(self.current_dir, f)) and 'kicad_' in f ]

		self.Project_files = []

		for f in file_list:
			if f.endswith(".kicad_pro"):
				self.Project_files.append(f)

		if not self.Project_files:
			return

		for pf in self.Project_files:
			pf_name = pf.replace('.kicad_pro', '')
			proj_sch_file = pf_name+'.kicad_sch'
			proj_pcb_file = pf_name+'.kicad_pcb'

			if proj_sch_file in file_list and proj_pcb_file in file_list:
				self.Project_list.append(pf_name)
		
		self.package_fab_output()

	def zipdir(self, path, ziph):
		for root, dirs, files in os.walk(path):
			for f in files:
				ziph.write(os.path.join(root,f),
							os.path.relpath(os.path.join(root,f),os.path.join(path, '..')))

	def read_conf_file(self, config_file_path):
		conf = configparser.ConfigParser()
		conf.read(config_file_path)

		FO_dir_list = []
		for pn in self.Project_list:
			FO_dir_list.append(conf[pn]["FODir"])

		return FO_dir_list

	def package_fab_output(self):

		# Gatting datetime
		now = dt.now()
		date = now.strftime("%b%d%Y")
		time_now = now.strftime("%H%M%S")

		# Setting up archive name
		outf_names = \
			[ pf+'_FabricationOutputs_'+date+'_'+time_now for pf in self.Project_list ]

		# Fabrication Output Directory Names
		# 
		# If only one prject exists, just setting up 'FabricationOutputs' directory.
		#
		# If multiple projects found, making FabricationOutputs_<project_name> dir for each project.
		#
		# But before that... we'll read in a ini file that matches
		# <project name> to Fabrication Output directory
		#
		
		config_file = os.path.join(self.current_dir, config_file_name)
		
		if not os.path.exists(config_file):
			conf = configparser.ConfigParser()
			if len(self.Project_list) == 1:
				conf[self.Project_list[0]] = {
					"FODir": "FabricationOutputs"
				}
			else:
				for pn in self.Project_list:
					conf[pn] = {
						"FODir": f"FabricationOutputs_{pn}"
					}

			with open(config_file, 'w') as fp:
				conf.write(fp)

		FO_Dirs = self.read_conf_file(config_file)

		for fodir in FO_Dirs:
			fo_dir_fullpath = os.path.join(self.current_dir, fodir)
			if not os.path.exists(fo_dir_fullpath):
				os.makedirs(fo_dir_fullpath)

		# Make output file directory as needed...
		OutfDir = os.path.join(self.current_dir, 'Communications')
		if not os.path.exists(OutfDir):
			os.makedirs(OutfDir)

		# Now compress the Fabrication Output directories...
		for pn, FOdir, zippedFile in zip(self.Project_list, FO_Dirs, outf_names):

			FOdir_path = os.path.join(self.current_dir, FOdir)

			BOM_csv = pn+'.csv'
			STEP_file = pn+'.step'
			IPC_2581_file = pn+'.xml'
			IPC_D_356 = pn+'.d356'

			additional_files = [ BOM_csv, STEP_file, IPC_2581_file, IPC_D_356 ]

			if self.archive_type == 'zip':
				zipped_file_path = os.path.join(OutfDir, zippedFile)
				with zipfile.ZipFile(
					zipped_file_path+'.'+self.archive_type, 'w', 
					zipfile.ZIP_DEFLATED,
					compresslevel=9) as zipf:
					self.zipdir(FOdir_path, zipf)
				
				# Adding some additional fabrication output files...
				for af in additional_files:
					af_path = os.path.join(self.current_dir, af)
					if os.path.exists(af_path):
						with zipfile.ZipFile(zipped_file_path+'.zip', 'a') as zf:
							zf.write(af_path, arcname=af)


"""
	SettingsDialog

"""
class SettingsDialog(wx.Dialog):

	def __init__(self, parent):
		wx.Dialog.__init__(self, parent, title="Run Packaging!!")

		sizer = wx.BoxSizer()
		panel = self.init_panel()
		sizer.Add(panel)
		self.SetSizerAndFit(sizer)

		self.Center()
		self.Show()

	def init_panel(self):
		panel = wx.Panel(self)
		rows = wx.BoxSizer(orient=wx.VERTICAL)

		buttons = wx.BoxSizer()
		button_cancel = wx.Button(panel, id=wx.ID_CANCEL, label="Cancel", size=(85,26))
		buttons.Add(button_cancel, flag=wx.ALL | wx.ALIGN_CENTER, border=5)
		button_package = wx.Button(panel, id=wx.ID_OK, label="Package", size=(85,26))
		button_package.Bind(wx.EVT_BUTTON, self.on_package)
		buttons.Add(button_package, flag=wx.ALL | wx.ALIGN_CENTER, border=5)

		rows.Add(buttons, flag=wx.ALL | wx.ALIGN_RIGHT, border=5)

		panel.SetSizer(rows)

		return panel

	def on_package(self, event):
		self.EndModal(wx.OK)







