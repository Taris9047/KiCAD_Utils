import os
import pcbnew
import wx

from .PackageFabOutput import PKGFOutput, SettingsDialog

__version__ = "0.0.1"

"""
	Class for KiCAD Plugin interface read-in

"""
class PackageFabOutputAction(pcbnew.ActionPlugin):
	
	def defaults(self):
		self.name = "Package Fabrication Output"
		self.category = "Export"
		self.description = \
            "Packages contents in fabrication output files into zip."
		self.show_toolbar_button = True
		self.icon_file_name = \
            os.path.join(os.path.dirname(__file__), 'icon.png')

	def Run(self):
		with SettingsDialog(None) as dlg:
			if dlg.ShowModal() == wx.OK:
				pf = PKGFOutput()
			

PackageFabOutputAction().register()