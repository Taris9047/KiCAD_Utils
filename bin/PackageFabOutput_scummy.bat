@echo off
setlocal

set CB_PROJ_NAME="A-STEP_CB"
set FB_PROJ_NAME="A-STEP_FlexBus"
set AG_PROJ_NAME="GECCO_Adapter"

for /f "delims=" %%# in ('powershell get-date -format "{MMMddyyyy}"') do @set currdate=%%#

for /f "delims=" %%# in ('powershell get-date -format "{HHmmss}"') do @set currtime=%%#

set CBsourceFolder=".\FabricationOutputs"
if not exist %CBsourceFolder% (
    mkdir %CBsourceFolder% 2> NUL
)

set FBsourceFolder=".\FabricationOutputs_Fbus"
if not exist %FBsourceFolder% (
    mkdir %FBsourceFolder% 2> NUL
)

set AGsourceFolder=".\FabricationOutputs_AtoG"
if not exist %AGsourceFolder% (
    mkdir %AGsourceFolder% 2> NUL
)


set destFolder=".\Communications"
mkdir %destFolder% 2> NUL

set CBzipName=%CB_PROJ_NAME%_FabricationOutputs_%currdate%_%currtime%.zip
set CBzipFile=%destFolder%\\%CBzipName%
dir /b %CBsourceFolder%\ | zip %CBzipFile% %CBsourceFolder%\*.* | echo "No files in CB Fabrication Output Dir."
dir /b %CBzipFile% | zip -gr %CBzipFile% ".\\%CB_PROJ_NAME%.csv" | echo "%CBzipFile% not found. Skipping"

set FBzipName=%FB_PROJ_NAME%_FabricationOutputs_%currdate%_%currtime%.zip
set FBzipFile=%destFolder%\\%FBzipName%
dir /b %FBsourceFolder%\ | zip %FBzipFile% %FBsourceFolder%\*.* | echo "No files in FBus Fabrication Output Dir."
dir /b %FBzipFile% | zip -gr %FBzipFile% ".\\%FB_PROJ_NAME%.csv" | echo "%FBzipFile% not found. Skipping"

set AGzipName=%AG_PROJ_NAME%_FabricationOutputs_%currdate%_%currtime%.zip
set AGzipFile=%destFolder%\\%AGzipName%
dir /b %AGsourceFolder%\ | zip %AGzipFile% %AGsourceFolder%\*.* | echo "No files in FBus Fabrication Output Dir."
dir /b %AGzipFile% | zip -gr %AGzipFile% ".\\%AG_PROJ_NAME%.csv" | echo "%AGzipFile% not found. Skipping"


echo "Jobs done..."