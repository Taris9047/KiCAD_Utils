@echo off
setlocal EnableDelayedExpansion

rem Project names must not have space!!
set ProjectList=A-STEP_CB A-STEP_FlexBus GECCO_Adapter
set n=0
for %%a in (%ProjectList%) do (
	set VPList[!n!]=%%a
	set /A n+=1
)

rem Corresponding fabrication output directories. 
set ProjectFODir=FabricationOutputs FabricationOutputs_FBus FabricationOutputs_AtoG
set n=0
for %%a in (%ProjectFODir%) do (
	set VFODir[!n!]=%%a
	set /A n+=1
)

rem Get Array length
set len=!n!
set /A len-=1

rem Setting up archive destination folder
set DestFolder=.\Communications
if not exist %DestFolder% (
	mkdir %DestFolder% 2> NUL
)

rem Setting up time and date
for /f "delims=" %%# in ('powershell get-date -format "{MMMddyyyy}"') do @set currdate=%%#
for /f "delims=" %%# in ('powershell get-date -format "{HHmmss}"') do @set currtime=%%#

rem Making output directory if needed.
for %%d in (%ProjectFODir%) do (
	if not exist %%d (
		echo "Making Output Directory: %%d"
		mkdir %%d 2> NUL
	)
)

rem Actually making archives
for /L %%i in (0,1,%len%) do (

	set zFileName=!VPList[%%i]!_FabricationOutputs_%currdate%_%currtime%.zip
	rem echo "Archive Name: !zFileName!"
	rem echo "Archive Name Direct Write: %%p_FabricationOutputs_%currdate%_%currtime%.zip"

	set zFilePath=%DestFolder%\!zFileName!
	echo "Archive Path: !zFilePath!"

	set out_FODir=!VFODir[%%i]!
	echo "out_FODir: !out_FODir!"

	set proj_filename=!VPList[%%i]!

	dir /b !out_FODir!\ | zip !zFilePath! !out_FODir!\*.* || echo "No files in !out_FODir! Dir."
	dir /b !zFilePath! | zip -gr !zFilePath! ".\\!proj_filename!.csv" || echo "!zFilePath! or !VPList[%%n]!.csv BOM file Not found! Skipping..."	 
)

echo "Jobs finished..."