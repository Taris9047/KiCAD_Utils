#!/usr/bin/env python

import os
import sys
import zipfile
import shutil
from datetime import datetime as dt

sys.dont_write_bytecode = True

current_dir = os.getcwd()

archive_type = 'zip'

Project_list = []

# Get project names from this directory.
file_list = [ f for f in os.listdir() if os.path.isfile(f) ]

Project_files = []
for f in file_list:
	if f.endswith(".kicad_pro"):
		Project_files.append(f)

if not Project_files:
	print("No project files found!! Exiting!")
	sys.exit(0)

for pf in Project_files:
	pf_name = pf.replace('.kicad_pro', '')
	proj_sch_file = pf_name+'.kicad_sch'
	proj_pcb_file = pf_name+'.kicad_pcb'

	if proj_sch_file in file_list and proj_pcb_file in file_list:
		print("Project '{}'' found".format(pf_name))
		Project_list.append(pf_name)


# Gatting datetime
now = dt.now()
date = now.strftime("%m%d%Y")
time_now = now.strftime("%H%M%S")

# Setting up archive name
outf_names = \
	[ pf+'_FabricationOutputs_'+date+'_'+time_now for pf in Project_list ]

# Fabrication Output Directory Names
# 
# If only one prject exists, just setting up 'FabricationOutputs' directory.
#
# If multiple projects found, making FabricationOutputs_<project_name> dir for each project.
#
FO_Dirs = []
if len(Project_list) == 1:
	FO_Dirs = ['FabricationOutputs']
else:
	for pn in Project_list:
		FO_Dirs.append("FabricationOutputs_"+pn)

for fodir in FO_Dirs:
	fo_dir_fullpath = os.path.join(current_dir, fodir)
	if not os.path.exists(fo_dir_fullpath):
		print("Fabrication output directory not found! Making one..")
		print("Making '{}'".format(fo_dir_fullpath))
		os.mkdir(fo_dir_fullpath)

# Make output file directory as needed...
OutfDir = os.path.join(current_dir, 'Communications')
if not os.path.exists(OutfDir):
	os.mkdir(OutfDir)


# Now compress the Fabrication Output directories...
for pn, FOdir, zippedFile in zip(Project_list, FO_Dirs, outf_names):
	print(f"Zipping Fabrication outputs: '{FOdir}'")
	zipped_file_path = os.path.join(OutfDir, zippedFile)
	shutil.make_archive(zipped_file_path, archive_type, os.path.join('.',FOdir))

	BOM_csv = pn+'.csv'
	STEP_file = pn+'.step'

	if archive_type == 'zip':
		if os.path.exists(os.path.join(current_dir,BOM_csv)):
			print(f"Adding BOM: {BOM_csv}")
			with zipfile.ZipFile(zipped_file_path+'.zip', 'a') as zf:
				zf.write(BOM_csv, arcname=BOM_csv)
		if os.path.exists(os.path.join(current_dir, STEP_file)):
			print(f"Adding STEP: {STEP_file}")
			with zipfile.ZipFile(zipped_file_path+'.zip', 'a') as zf:
				zf.write(STEP_file, arcname=STEP_file)

print("Jobs Finished!!")
